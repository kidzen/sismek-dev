<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'oci:dbname=//localhost:1521/mpsp12c;charset=UTF8',
            'username' => 'sismektest',
            'password' => 'sismek',
            'enableSchemaCache' => true,
            'enableQueryCache' => true,
        ],
    ],
];
