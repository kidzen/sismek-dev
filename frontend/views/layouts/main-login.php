<?php

use backend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

frontend\assets\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <!--<body class="img-layout">-->
    <!--<body class="skin-blue layout-top-nav">-->
    <body class="skin-blue layout-top-nav" data-gr-c-s-loaded="true">


        <?php $this->beginBody() ?>
        <div class="full-screen">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="../../index2.html" class="navbar-brand"><b>SISMECH</b> Sistem Mekanikal MPSP</a>
                        </div>

                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- Messages: style can be found in dropdown.less-->
                                <li class="dropdown messages-menu">
                                    <!-- Menu toggle button -->
                                    <?= $content ?>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-custom-menu -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>
            <!-- Full Width Column -->
            <div class="full-screen">
                <!--<div class="content-wrapper">-->
                <!--<div class="wrapper">-->
                <?php
                echo \yii\bootstrap\Carousel::widget([
                    'items' => [
                        // the item contains only the image
//                        '<img src="img/MPSP1.jpg"/>',
//                        '<img src="img/MPSP2.jpg"/>',
//                        '<img src="img/MPSP3.jpg"/>',
//                        '<img src="img/MPSP4.jpg"/>',
                        // equivalent to the above
//                        ['content' => '<img src="http://twitter.github.io/bootstrap/assets/img/bootstrap-mdo-sfmoma-02.jpg"/>'],
                        // the item contains both the image and the caption
                        [
                            'content' => '<img src="img/MPSP1.jpg" style="height: 100%;width: 100%;" />',
//                            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                                'options' => ['class' => 'full-screen'],
                        ],
                        [
                            'content' => '<img src="img/MPSP2.jpg" style="height: 100%;width: 100%;" />',
//                            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                                'options' => ['class' => 'full-screen'],
                        ],
                        [
                            'content' => '<img src="img/MPSP3.jpg" style="height: 100%;width: 100%;" />',
//                            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                                'options' => ['class' => 'full-screen'],
                        ],
                        [
                            'content' => '<img src="img/MPSP4.jpg" style="height: 100%;width: 100%;" />',
//                            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                                'options' => ['class' => 'full-screen'],
                        ],
                    ],
                    'options' => ['class' => 'full-screen'],
                ]);
                ?>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
