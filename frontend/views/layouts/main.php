<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
//var_dump(Yii::$app->user->role);die();
//isset(Yii::$app->user->identity->roles->NAME)? Yii::$app->user->identity->roles->NAME : null;
//isset(Yii::$app->user->identity->roles->NAME)? Yii::$app->user->identity->roles->NAME : null;
//$username = Yii::$app->user->name ? Yii::$app->user->name : 'Guest';
//$activeBool = Yii::$app->user->id ? 'Online' : 'Offline';
//$roles = Yii::$app->user->role;


//dmstr\web\AdminLteAsset::register($this);
frontend\assets\AdminLteAsset::register($this);
//frontend\assets\AppAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist');
//var_dump($directoryAsset);die();
//var_dump(readdir(opendir($directoryAsset)));
//die();
//$file = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist/css/Pdf.css');
//$file = Yii::getAlias('@frontend/assets/dist/css/Pdf.css');
//$file = Yii::getAlias('@web/uploads/profile_pic/admin1.jpg');
$file = Yii::getAlias('@frontend/assets/dist/pdf_template/KEW.PS-8.pdf');
//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?php echo $directoryAsset; ?>/img/favicon.ico" type="image/x-icon" />
        <?php $this->head() ?>
    </head>
    <!--<body class="hold-transition skin-purple-light sidebar-mini">-->
    <!--<body class="hold-transition skin-blue-light fixed sidebar-mini sidebar-collapse">-->
    <body class="skin-blue-light fixed sidebar-mini sidebar-collapse">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <?php
//            echo \yii\bootstrap\Carousel::widget([
//                'items' => [
//                    // the item contains only the image
//                    '<img src="http://twitter.github.io/bootstrap/assets/img/bootstrap-mdo-sfmoma-01.jpg"/>',
//                    // equivalent to the above
//                    ['content' => '<img src="http://twitter.github.io/bootstrap/assets/img/bootstrap-mdo-sfmoma-02.jpg"/>'],
//                    // the item contains both the image and the caption
//                    [
//                        'content' => '<img src="http://twitter.github.io/bootstrap/assets/img/bootstrap-mdo-sfmoma-03.jpg"/>',
//                        'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                        'options' => [],
//                    ],
//                ]
//            ]);
            ?>
            <?=
            $this->render(
                    'header.php', [
                'directoryAsset' => $directoryAsset,
                'username' => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
                'activeBool' => Yii::$app->user->status,
                'roles' => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
                    ]
            )
            ?>

            <?=
            $this->render(
                    'left.php', [
                'directoryAsset' => $directoryAsset,
                'username' => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
                'activeBool' => Yii::$app->user->status,
                'roles' => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
                    ]
            )
            ?>

            <?=
            $this->render(
                    'content.php', [
                'content' => $content,
                'directoryAsset' => $directoryAsset,
                'username' => isset(Yii::$app->user->username) ? Yii::$app->user->username : 'Guest',
                'activeBool' => Yii::$app->user->status,
                'roles' => isset(Yii::$app->user->role) ? Yii::$app->user->role : 'Guest',
                    ]
            )
            ?>

        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
