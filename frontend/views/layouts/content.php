<?php

use yii\widgets\Breadcrumbs;
//use dmstr\widgets\Alert;
//use Yii;
use kartik\widgets\Growl;
use yii\helpers\Html;
use yii\bootstrap\Modal;

//foreach(PDO::getAvailableDrivers() as $driver){
//    echo $driver, '\n';
//}
//die();
Modal::begin([
    'id' => 'modalTutor',
    'size' => 'modal-lg',
    'header' => '<h2>Cara-cara Penggunaan Sistem' . Html::a('<i class="glyphicon glyphicon-print"></i> Print', ['site/tutor-pdf'], [
        'class' => 'pull-right btn btn-info',
        'target' => '_blank',
        'data-toggle' => 'tooltip',
        'title' => 'Print'
    ]) . '</h2>'
]);
echo "<div id='modalTutorContent'></div>";
Modal::end();
?>
<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
                    <!--<h1><?= $this->blocks['content-header']; ?></h1>-->
        <?php } else { ?>
            <!--            <h1>
            <?php
            if ($this->title !== null) {
                echo \yii\helpers\Html::encode($this->title);
            }
//                else {
//                    echo \yii\helpers\Inflector::camel2words(
//                        \yii\helpers\Inflector::id2camel($this->context->module->id)
//                    );
//                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
//                } 
            ?>
                        </h1>-->

        <?php } ?>


    </section>
    <!--    <div class="animate11"> hai
        </div>-->
<!--    <section id="content-nav">
        <div class="row placeholders delay">
            <div class="col-xs-6 col-sm-2 placeholder delay-child">
                <div class="nav-divin" style="text-align: center">
                    color: #00a65a
                    <?= Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-download" aria-hidden="true"></i>'), ['entry/create'], ['class' => 'cnavin']); ?>
                    <h4 class="text-muted">Permohonan Masuk</h4>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder delay-child">
                <div class="nav-divout" style="text-align: center">
                    <?= Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-upload" aria-hidden="true"></i>'), ['request/create'], ['class' => 'cnavout']); ?>
                    <h4 class="text-muted">Permohonan Keluar</h4>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder delay-child">
                <div class="nav-divapp" style="text-align: center">
                    <?= Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-gavel" aria-hidden="true"></i>'), ['request/index'], ['class' => 'cnavapp']); ?>
                    <h4 class="text-muted">Pengesahan</h4>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder delay-child">
                <div class="nav-divinv" style="text-align: center">
                    <?= Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-archive" aria-hidden="true"></i>'), ['inventory-items/index'], ['class' => 'cnavinv']); ?>
                    <h4 class="text-muted">Inventori</h4>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder delay-child">
                <div class="nav-divinv" style="text-align: center">
                    <?= Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-stack-overflow" aria-hidden="true"></i>'), ['report/index'], ['class' => 'cnavinv']); ?>
                    <h4 class="text-muted">Laporan</h4>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2 placeholder delay-child">
                <div class="nav-divinv" style="text-align: center">
                    <?php echo Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-info-circle" aria-hidden="true"></i>'), false, ['class' => 'tutorButton cnavinfo']); ?>
                    <?php // echo  Html::a(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-info-circle" aria-hidden="true"></i>'), ['site/tutor'], ['class' => 'tutorButton cnavinfo']); ?>
                    <?php // echo Html::button(Yii::t('app', '<i style="text-align: center;font-size: 4em;" class="fa fa-stack-overflow" aria-hidden="true"></i>'), ['value'=>'site/tutor','class' => 'btn cnavinv','id' => 'tutorButton']); ?>
                    <h4 class="text-muted">Info</h4>
                </div>
            </div>

        </div>
    </section>-->
    <div class="info-box-content">

    </div>
    <section class="content">
        <?php
        $delay = 0;
        foreach (Yii::$app->session->getAllFlashes() as $message):;
            echo \kartik\widgets\Growl::widget([
                'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                'title' => (!empty($message['title'])) ? Html::encode($message['title']) : null,
                'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                'body' => (!empty($message['message'])) ? $message['message'] : 'Message Not Set!',
                'showSeparator' => true,
                'delay' => $delay, //This delay is how long before the message shows
                'pluginOptions' => [
                    'showProgressbar' => true,
                    'delay' => (!empty($message['duration'])) ? $message['duration'] + $delay : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
            $delay = $delay + 1000;
            ?>
        <?php endforeach; ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; <?=
        date('Y');
        echo Html::a(' Majlis Perbandaran Seberang Perai', false)
        ?>.</strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->
<!--<aside class="control-sidebar control-sidebar-dark">
     Create the tabs 
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
     Tab panes 
    <div class="tab-content">
         Home tab content 
        <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                            <p>Will be 23 on April 24th</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-user bg-yellow"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                            <p>New phone +1(800)555-1234</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                            <p>nora@example.com</p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <i class="menu-icon fa fa-file-code-o bg-green"></i>

                        <div class="menu-info">
                            <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                            <p>Execution time 5 seconds</p>
                        </div>
                    </a>
                </li>
            </ul>
             /.control-sidebar-menu 

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class='control-sidebar-menu'>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Custom Template Design
                            <span class="label label-danger pull-right">70%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Update Resume
                            <span class="label label-success pull-right">95%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Laravel Integration
                            <span class="label label-waring pull-right">50%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                        </div>
                    </a>
                </li>
                <li>
                    <a href='javascript::;'>
                        <h4 class="control-sidebar-subheading">
                            Back End Framework
                            <span class="label label-primary pull-right">68%</span>
                        </h4>

                        <div class="progress progress-xxs">
                            <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                        </div>
                    </a>
                </li>
            </ul>
             /.control-sidebar-menu 

        </div>
         /.tab-pane 

         Settings tab content 
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
                <h3 class="control-sidebar-heading">General Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Report panel usage
                        <input type="checkbox" class="pull-right" checked/>
                    </label>

                    <p>
                        Some information about this general settings option
                    </p>
                </div>
                 /.form-group 

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Allow mail redirect
                        <input type="checkbox" class="pull-right" checked/>
                    </label>

                    <p>
                        Other sets of options are available
                    </p>
                </div>
                 /.form-group 

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Expose author name in posts
                        <input type="checkbox" class="pull-right" checked/>
                    </label>

                    <p>
                        Allow the user to show his name in blog posts
                    </p>
                </div>
                 /.form-group 

                <h3 class="control-sidebar-heading">Chat Settings</h3>

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Show me as online
                        <input type="checkbox" class="pull-right" checked/>
                    </label>
                </div>
                 /.form-group 

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Turn off notifications
                        <input type="checkbox" class="pull-right"/>
                    </label>
                </div>
                 /.form-group 

                <div class="form-group">
                    <label class="control-sidebar-subheading">
                        Delete chat history
                        <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                    </label>
                </div>
                 /.form-group 
            </form>
        </div>
         /.tab-pane 
    </div>
</aside> /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<!--<div class='control-sidebar-bg'></div>-->