<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\Carousel;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <div class="navbar-fixed-top navbar-inverse main-login-header">
                <div class="navbar-header">
                    <button data-target="#navbar-main" data-toggle="collapse" type="button" class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-header pull-left">
                        <?=
                        Html::a('<i class="glyphicon glyphicon-wrench"></i> ' . Yii::$app->params['apps-name'] . ' (' . Yii::$app->params['apps-short-name'] . ' ' . Yii::$app->params['apps-version'] . ')'
                                , ['/site/index']
                                , ['class' => 'navbar-brand', 'style' => 'color:white'])
                        ?>
                    </div>
                </div>
                <div id="navbar-main" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li style="margin:10px 40px 10px 10px;">
                            <?= $content ?>
                        </li>
                    </ul>
                </div>
            </div>

            <!--<div class="container-fluid">-->
            <?php
//            echo Carousel::widget([
//                'options' => ['height'=>'100%'],
//                'items' => [
//                    // the item contains only the image
////                        '<img src="' . Yii::getAlias('@web') . '/img/MPSP1.jpg"/>',
//                    // equivalent to the above
////                        ['content' => '<img src="' . Yii::getAlias('@web') . '/img/MPSP2.jpg"/>'],
//                    // the item contains both the image and the caption
//                    [
//                        'content' => Html::img(Yii::getAlias('@web') . '/img/MPSP1.jpg',['height'=>'100%']) ,
//                        'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                        'options' => ['height'=>'100%'],
//                    ],
//                    [
//                        'content' => Html::img(Yii::getAlias('@web') . '/img/MPSP2.jpg',['height'=>'100%']) ,
//                        'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                        'options' => [],
//                    ],
//                    [
//                        'content' => Html::img(Yii::getAlias('@web') . '/img/MPSP3.jpg',['height'=>'100%']) ,
//                        'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
//                        'options' => [],
//                    ],
//                ]
//            ]);
            ?>
            <!--</div>-->
        </div>

        <?php if (Yii::$app->params['showFooter']) { ?>
            <footer class="footer">
                <div class="container-fluid">
                    <p class="pull-left">&copy; <?= Yii::$app->params['company'] ?> <?= date('Y') ?></p>

                    <p class="pull-right"><?= Yii::powered() ?></p>
                </div>
            </footer>
        <?php } ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
<?php
$this->registerJs("$('.carousel').carousel({
  interval: 6000
});");
