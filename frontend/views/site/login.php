<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveField;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="site-login">
    <?php
    $form = ActiveForm::begin([
                'id' => 'login-form'
                , 'layout' => 'inline'
                , 'method' => 'post'
                , 'fieldConfig' => [
                    'labelOptions' => ['class' => '', 'style' => 'color:white'],
                    'enableError' => true,
                ]
    ]);
    ?>

    <?= $form->field($model, 'username', ['template' => '{label} {input}'])->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'password', ['template' => '{label} {input}'])->passwordInput()
    ?>

    <?php // echo $form->field($model, 'rememberMe')->inline()->checkbox()->label('Remember me', ['style' => 'color:white']) ?>
    <?= Html::submitButton('Login', ['class' => 'btn btn-info', 'name' => 'login-button']) ?>

    <?php ActiveForm::end(); ?>
</div>
