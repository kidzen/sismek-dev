<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
//    'bootstrap' => ['log', 'debug'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'notify' => [
            'class' => 'common\components\Notify',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'class' => 'common\components\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'assetManager' => [
//            'bundles'=>[
////                'yii\bootstrap\BootstrapAsset'=>[
////                    'css'=>[],
////                ],
////                'yii\bootstrap\BootstrapPluginAsset'=>[
////                    'css'=>[],
////                    'js'=>[],
////                ],
////                'yii\web\YiiAsset'=>[
////                    'css'=>[],
////                    'js'=>[],
////                ],
////                'yii\web\JqueryAsset'=>[
////                    'css'=>[],
////                    'js'=>[],
////                ],
//            ]
//            'linkAssets' => true,
             'forceCopy' => true,
//            'appendTimestamp' => true,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'access' => [
            'class' => 'common/component/AccessRule',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'rules' => [
//            ],
//        ],
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
//        'datecontrol' => [
//            'class' => '\kartik\datecontrol\Module',
//        ],
//        'dynagrid' => [
//            'class' => '\kartik\dynagrid\Module',
//        ],
        'debug' => ['class' => 'yii\debug\Module'], //remove on production
        'gii' => [
            'class' => 'yii\gii\Module',
//            'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
            'generators' => [ //here
                'crud' => [ // generator name
                    'class' => 'yii\gii\generators\crud\Generator', // generator class
                    'templates' => [ //setting for out templates
                        'myCrud' => '@frontend/components/templates/crud/default', // template name => path to template
                    ]
                ]
            ],
        ],
    ],
    'params' => $params,
];
